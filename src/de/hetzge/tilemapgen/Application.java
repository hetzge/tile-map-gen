package de.hetzge.tilemapgen;

import java.io.File;
import java.util.Random;

import de.hetzge.tilemapgen.base.impl.SimpleSize;
import de.hetzge.tilemapgen.map.def.TileMap;
import de.hetzge.tilemapgen.map.impl.TileMapDebugFormat;
import de.hetzge.tilemapgen.map.impl.TileMapJsonFormat;

public class Application {
	public static void main(String[] args) {
		final TileMap tileMap = new SGameTileMap(new SimpleSize(1000), new Random(123));

		new TileMapJsonFormat().writeToFile(new File("out.map"), tileMap);
		new TileMapDebugFormat().writeToFile(new File("out.debug"), tileMap);
		
		System.out.println("done");
	}
}
