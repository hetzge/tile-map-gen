package de.hetzge.tilemapgen.generator.impl;

import java.util.Random;

import de.hetzge.tilemapgen.base.def.Grid;
import de.hetzge.tilemapgen.base.def.Percent;
import de.hetzge.tilemapgen.base.def.Position;
import de.hetzge.tilemapgen.base.def.Size;
import de.hetzge.tilemapgen.base.impl.SimplePercent;
import de.hetzge.tilemapgen.generator.def.Chunk;
import javaslang.control.Option;

public class BorderValueChunkGrid implements Grid<Chunk> {

	private final Size size;
	private final Percent value;
	private final Random random;

	public BorderValueChunkGrid(Size size, Percent value, Random random) {
		this.size = size;
		this.value = value;
		this.random = random;
	}

	@Override
	public Size size() {
		return this.size;
	}

	@Override
	public Option<Chunk> get(Position position) {

		if (this.size.isBorder(position)) {
			final Percent topLeft = position.x() == 0 || position.y() == 0 ? this.value : random();
			final Percent topRight = position.x() == this.size.width() - 1 || position.y() == 0 ? this.value : random();
			
			final Percent bottomLeft = position.x() == 0 || position.y() == this.size.height() - 1 ? this.value : random();
			final Percent bottomRight = position.x() == this.size.width() - 1 || position.y() == this.size.height() - 1 ? this.value : random();
			
			return Option.of(new SimpleChunk(topLeft, topRight, bottomLeft, bottomRight));
		} else {
			return Option.none();
		}
	}
	
	private Percent random() {
		return new SimplePercent(this.random.nextFloat() * 0.4f);
	}
}