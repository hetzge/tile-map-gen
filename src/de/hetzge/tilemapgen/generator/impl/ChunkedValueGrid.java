package de.hetzge.tilemapgen.generator.impl;

import java.util.Random;

import de.hetzge.tilemapgen.base.def.Grid;
import de.hetzge.tilemapgen.base.def.Percent;
import de.hetzge.tilemapgen.base.def.Position;
import de.hetzge.tilemapgen.base.def.Size;
import de.hetzge.tilemapgen.base.impl.SimplePosition;
import de.hetzge.tilemapgen.generator.def.Chunk;
import javaslang.control.Option;

public final class ChunkedValueGrid implements Grid<Percent> {

	private final Size size;
	private final Size chunkSize;
	private final Grid<Chunk> chunkGrid;

	public ChunkedValueGrid(Size size, Size chunkSize, Random random) {
		this(size, chunkSize, new ChunkGrid(size.divide(chunkSize), random));
	}

	public ChunkedValueGrid(Size size, Size chunkSize, Grid<Chunk> chunkGrid) {
		this.size = size;
		this.chunkSize = chunkSize;
		this.chunkGrid = chunkGrid;
	}

	@Override
	public Size size() {
		return this.size;
	}

	@Override
	public Option<Percent> get(Position position) {
		if (this.size.contains(position)) {

			final int x = (int) Math.floor(position.x() / (float) this.chunkSize.width());
			final int y = (int) Math.floor(position.y() / (float) this.chunkSize.height());
			return this.chunkGrid.get(new SimplePosition(x, y)).flatMap(chunk -> {
				final ChunkValueGrid chunkZoneGrid = new ChunkValueGrid(this.chunkSize, chunk);

				final int chunkX = position.x() % this.chunkSize.width();
				final int chunkY = position.y() % this.chunkSize.height();
				return chunkZoneGrid.get(new SimplePosition(chunkX, chunkY));
			});
		} else {
			return Option.none();
		}
	}

}
