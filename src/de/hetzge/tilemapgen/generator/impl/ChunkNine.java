package de.hetzge.tilemapgen.generator.impl;

import de.hetzge.tilemapgen.base.def.Grid;
import de.hetzge.tilemapgen.base.def.Nine;
import de.hetzge.tilemapgen.base.def.Position;
import de.hetzge.tilemapgen.base.def.Size;
import de.hetzge.tilemapgen.base.impl.AroundPosition;
import de.hetzge.tilemapgen.base.impl.Direction;
import de.hetzge.tilemapgen.base.impl.SimpleSize;
import de.hetzge.tilemapgen.generator.def.Chunk;
import javaslang.control.Option;

final class ChunkNine implements Nine<Chunk> {

	private final Grid<Chunk> chunkGrid;
	private final Position position;

	public ChunkNine(Grid<Chunk> chunkGrid, Position position) {
		this.chunkGrid = chunkGrid;
		this.position = position;
	}

	@Override
	public Option<Chunk> get(Direction direction) {
		return get(new AroundPosition(this.position, direction));
	}

	@Override
	public Option<Chunk> get(Position position) {
		return this.chunkGrid.get(position);
	}

	@Override
	public Size size() {
		return new SimpleSize(3);
	}
}
