package de.hetzge.tilemapgen.generator.impl;

import java.util.Random;

import de.hetzge.tilemapgen.base.def.Grid;
import de.hetzge.tilemapgen.base.def.MutableGrid;
import de.hetzge.tilemapgen.base.def.Position;
import de.hetzge.tilemapgen.base.def.Size;
import de.hetzge.tilemapgen.base.impl.SimpleMutableGrid;
import de.hetzge.tilemapgen.generator.def.Chunk;
import javaslang.control.Option;

public final class ChunkGrid implements Grid<Chunk> {

	private final MutableGrid<Chunk> chunks;

	private final Random random;

	public ChunkGrid(Size size, Random random) {
		this(new SimpleMutableGrid<>(size), random);
	}

	public ChunkGrid(Grid<Chunk> chunks, Random random) {
		this.chunks = new SimpleMutableGrid<>(chunks);
		this.random = random;
	}

	@Override
	public Size size() {
		return this.chunks.size();
	}

	@Override
	public Option<Chunk> get(Position position) {
		if (this.chunks.size().contains(position)) {

			final Option<Chunk> chunksOption = this.chunks.get(position);

			if (chunksOption.isDefined()) {
				return chunksOption;
			} else {
				final Chunk chunk = new SimpleChunk(new NineChunk(new ChunkNine(this.chunks, position), this.random));

				this.chunks.set(position, chunk);

				return Option.of(chunk);
			}
		} else {
			return Option.none();
		}
	}
}
