package de.hetzge.tilemapgen.generator.impl;

import java.util.Random;
import java.util.function.Function;

import de.hetzge.tilemapgen.base.def.Nine;
import de.hetzge.tilemapgen.base.def.Percent;
import de.hetzge.tilemapgen.base.impl.Direction;
import de.hetzge.tilemapgen.base.impl.SimplePercent;
import de.hetzge.tilemapgen.generator.def.Chunk;

public final class NineChunk implements Chunk {

	private final Nine<Chunk> nine;
	private final Random random;

	public NineChunk(Nine<Chunk> nine, Random random) {
		this.nine = nine;
		this.random = random;
	}

	@Override
	public Percent topLeft() {
		return cornerValue(Direction.LEFT, Chunk::topRight, Direction.TOP, Chunk::bottomLeft, Direction.TOP_LEFT, Chunk::bottomRight);
	}

	@Override
	public Percent topRight() {
		return cornerValue(Direction.RIGHT, Chunk::topLeft, Direction.TOP, Chunk::bottomRight, Direction.TOP_RIGHT, Chunk::bottomLeft);
	}

	@Override
	public Percent bottomLeft() {
		return cornerValue(Direction.LEFT, Chunk::bottomRight, Direction.BOTTOM, Chunk::topLeft, Direction.BOTTOM_LEFT, Chunk::topRight);
	}

	@Override
	public Percent bottomRight() {
		return cornerValue(Direction.RIGHT, Chunk::bottomLeft, Direction.BOTTOM, Chunk::topRight, Direction.BOTTOM_RIGHT, Chunk::topLeft);
	}

	private final Percent cornerValue(Direction a, Function<Chunk, Percent> valueA, Direction b, Function<Chunk, Percent> valueB, Direction c, Function<Chunk, Percent> valueC) {
		return this.nine.get(a).map(valueA).orElse(() -> this.nine.get(b).map(valueB).orElse(() -> this.nine.get(c).map(valueC))).getOrElse(() -> new SimplePercent(this.random.nextFloat()));
	}
}
