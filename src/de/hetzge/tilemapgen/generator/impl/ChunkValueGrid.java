package de.hetzge.tilemapgen.generator.impl;

import de.hetzge.tilemapgen.base.def.Grid;
import de.hetzge.tilemapgen.base.def.Percent;
import de.hetzge.tilemapgen.base.def.Position;
import de.hetzge.tilemapgen.base.def.Size;
import de.hetzge.tilemapgen.base.impl.SimplePercent;
import de.hetzge.tilemapgen.generator.def.Chunk;
import javaslang.control.Option;

public class ChunkValueGrid implements Grid<Percent> {

	private final Size size;
	private final Chunk chunk;

	public ChunkValueGrid(Size size, Chunk chunk) {
		this.size = size;
		this.chunk = chunk;
	}

	@Override
	public Size size() {
		return this.size;
	}

	@Override
	public Option<Percent> get(Position position) {
		final float horizontalPercentage = position.x() / ((float) this.size.width() - 1);
		final float verticalPercentage = position.y() / ((float) this.size.height() - 1);

		final float top = this.chunk.topLeft().value() + (this.chunk.topRight().value() - this.chunk.topLeft().value()) * horizontalPercentage;
		final float bottom = this.chunk.bottomLeft().value() + (this.chunk.bottomRight().value() - this.chunk.bottomLeft().value()) * horizontalPercentage;
		
		final float value = top + (bottom - top) * verticalPercentage;
	
		return Option.some(new SimplePercent(value));
	}

}
