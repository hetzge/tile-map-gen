package de.hetzge.tilemapgen.generator.impl;

import de.hetzge.tilemapgen.base.def.Percent;
import de.hetzge.tilemapgen.generator.def.Chunk;

public final class SimpleChunk implements Chunk {

	private final Percent topLeft;
	private final Percent topRight;
	private final Percent bottomLeft;
	private final Percent bottomRight;

	public SimpleChunk(Chunk chunk) {
		this(chunk.topLeft(), chunk.topRight(), chunk.bottomLeft(), chunk.bottomRight());
	}

	public SimpleChunk(Percent topLeft, Percent topRight, Percent bottomLeft, Percent bottomRight) {
		this.topLeft = topLeft;
		this.topRight = topRight;
		this.bottomLeft = bottomLeft;
		this.bottomRight = bottomRight;
	}

	@Override
	public Percent topLeft() {
		return this.topLeft;
	}

	@Override
	public Percent topRight() {
		return this.topRight;
	}

	@Override
	public Percent bottomLeft() {
		return this.bottomLeft;
	}

	@Override
	public Percent bottomRight() {
		return this.bottomRight;
	}

	@Override
	public String toString() {
		return String.format("SimpleChunk [topLeft=%s, topRight=%s, bottomLeft=%s, bottomRight=%s]", this.topLeft, this.topRight, this.bottomLeft, this.bottomRight);
	}
}
