package de.hetzge.tilemapgen.generator.def;

import de.hetzge.tilemapgen.base.def.Percent;

public interface Chunk {
	Percent topLeft();

	Percent topRight();

	Percent bottomLeft();

	Percent bottomRight();
}
