package de.hetzge.tilemapgen.map.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import de.hetzge.tilemapgen.base.def.Position;
import de.hetzge.tilemapgen.base.def.Size;
import de.hetzge.tilemapgen.base.impl.SizePositions;
import de.hetzge.tilemapgen.base.impl.Utils;
import de.hetzge.tilemapgen.map.def.Layer;
import de.hetzge.tilemapgen.map.def.Rule;
import de.hetzge.tilemapgen.map.def.TileMap;
import de.hetzge.tilemapgen.map.def.Zone;
import javaslang.control.Option;

public final class RuledTileMap implements TileMap {

	private final TileMap tileMap;
	private final List<Rule> rules;

	public RuledTileMap(TileMap tileMap, List<Rule> rules) {
		this.tileMap = tileMap;
		this.rules = rules;
	}

	@Override
	public List<Layer> layers() {
		final List<Layer> layers = this.tileMap.layers();

		if (!layers.isEmpty()) {
			return Utils.reverse(apply(Utils.reverse(layers)));
		} else {
			return layers;
		}
	}

	private List<Layer> apply(List<Layer> layers) {
		final List<Layer> head = apply0(layers).map(Arrays::asList).getOrElse(Collections.emptyList());
		final List<Layer> tail = Utils.tail(layers).map(this::apply).getOrElse(Collections.emptyList());
		return Utils.concat(head, tail);
	}

	private Option<Layer> apply0(List<Layer> layers) {
		if (layers.size() >= 2) {

			for (Layer headLayer : Utils.head(layers)) {
				final Zone headZone = headLayer.zone();
				final Size size = headLayer.size();
				final SimpleLayer newLayer = new SimpleLayer(headLayer.zone(), size);
				for (List<Layer> tailLayers : Utils.tail(layers)) {
					for (Position position : new SizePositions(size)) {
						final Option<Zone> tailZoneOption = tailZone(tailLayers, position);
						if (tailZone(tailLayers, position).isDefined() && headLayer.is(position)) {
							final Zone tailZone = tailZoneOption.get();
							if (this.rules.stream().allMatch(rule -> rule.allowed(headZone, tailZone))) {
								newLayer.set(position, headLayer.is(position));
							}
						} else {
							newLayer.set(position, headLayer.is(position));
						}
					}
				}
				return Option.of(newLayer);
			}

			throw new IllegalStateException();

		} else if (layers.size() == 1) {
			return Option.some(layers.get(0));
		} else {
			return Option.none();
		}
	}

	private Option<Zone> tailZone(List<Layer> tailLayers, Position position) {
		return Option.ofOptional(tailLayers.stream().filter(it -> it.is(position)).findFirst().map(Layer::zone));
	}
}
