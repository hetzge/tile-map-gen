package de.hetzge.tilemapgen.map.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;

import de.hetzge.tilemapgen.base.def.Grid;
import de.hetzge.tilemapgen.base.def.Position;
import de.hetzge.tilemapgen.base.impl.SizePositions;
import de.hetzge.tilemapgen.base.impl.Utils;
import de.hetzge.tilemapgen.map.def.Layer;
import de.hetzge.tilemapgen.map.def.TileMap;
import de.hetzge.tilemapgen.map.def.Zone;
import javaslang.control.Option;

public final class SeparateZonesTileMap implements TileMap {

	private final List<Zone> zones;
	private final Grid<Zone> zoneGrid;

	public SeparateZonesTileMap(List<Zone> zones, Grid<Zone> zoneGrid) {
		this.zones = zones;
		this.zoneGrid = zoneGrid;
	}

	@Override
	public List<Layer> layers() {
		final Map<Zone, SimpleLayer> layersByZone = this.zones.stream().collect(Utils.toLinkedMap(zone -> zone, zone -> new SimpleLayer(zone, this.zoneGrid.size())));
		
		// TODO
		final List<Position> positions = new SizePositions(this.zoneGrid.size()).toList();
		Collections.shuffle(positions, new Random(12345));
		
		for (Position position : positions) {
			for (Zone zone : this.zoneGrid.get(position)) {
				for (SimpleLayer layer : Option.of(layersByZone.get(zone))) {
					layer.set(position, true);
				}
			}
		}

		return new ArrayList<>(layersByZone.values());
	}
}
