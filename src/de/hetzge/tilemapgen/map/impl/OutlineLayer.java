package de.hetzge.tilemapgen.map.impl;

import de.hetzge.tilemapgen.base.def.Position;
import de.hetzge.tilemapgen.base.def.Size;
import de.hetzge.tilemapgen.base.impl.AdvancedAroundPositions;
import de.hetzge.tilemapgen.base.impl.Utils;
import de.hetzge.tilemapgen.map.def.Layer;
import de.hetzge.tilemapgen.map.def.Zone;

public final class OutlineLayer implements Layer {

	private final Layer layer;

	public OutlineLayer(Layer layer) {
		this.layer = layer;
	}

	@Override
	public Size size() {
		return this.layer.size();
	}

	@Override
	public Zone zone() {
		return this.layer.zone();
	}

	@Override
	public boolean is(Position position) {
		final boolean is = this.layer.is(position);
		return is && (Utils.toList(new AdvancedAroundPositions(position, size()).iterator()).stream().anyMatch(aroundPosition -> {
			final boolean isAround = this.layer.is(aroundPosition);
			return is != isAround;
		}) || size().isBorder(position));
	}
}
