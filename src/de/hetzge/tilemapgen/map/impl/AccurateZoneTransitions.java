package de.hetzge.tilemapgen.map.impl;

import java.util.List;

import de.hetzge.tilemapgen.base.def.Percent;
import de.hetzge.tilemapgen.map.def.Zone;
import de.hetzge.tilemapgen.map.def.ZoneTransitions;
import javaslang.control.Option;

/**
 * Consistent distribution of zones.
 */
public final class AccurateZoneTransitions implements ZoneTransitions {

	private final List<Zone> zones;

	public AccurateZoneTransitions(List<Zone> zones) {
		this.zones = zones;
	}

	@Override
	public Option<Zone> zone(Percent value) {
		final int lastIndex = this.zones.size() - 1;
		final int index = (int) (Math.floor(value.value() / (1f / this.zones.size())));
		return Option.some(this.zones.get(Math.min(index, lastIndex)));
	}
}
