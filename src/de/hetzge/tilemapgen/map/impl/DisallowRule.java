package de.hetzge.tilemapgen.map.impl;

import de.hetzge.tilemapgen.map.def.Rule;
import de.hetzge.tilemapgen.map.def.Zone;
import javaslang.collection.HashMap;
import javaslang.collection.HashSet;
import javaslang.collection.List;
import javaslang.collection.Map;
import javaslang.collection.Set;

public final class DisallowRule implements Rule {

	private final Map<Zone, Set<Zone>> disallowedZonesByZone;

	public DisallowRule() {
		this(HashMap.empty());
	}

	private DisallowRule(Map<Zone, Set<Zone>> disallowedZonesByZone) {
		this.disallowedZonesByZone = disallowedZonesByZone;
	}

	@Override
	public boolean allowed(Zone zone, Zone onTopOf) {
		return !this.disallowedZonesByZone.get(onTopOf).getOrElse(HashSet.empty()).contains(zone);
	}

	public DisallowRule with(Zone zone, Zone... disallowedZones) {
		return new DisallowRule(this.disallowedZonesByZone.put(zone, HashSet.of(disallowedZones)));
	}

	public DisallowRule with(List<Zone> zones, Zone... disallowedZones) {
		DisallowRule rule = this;
		for (Zone zone : zones) {
			rule = rule.with(zone, disallowedZones);
		}
		return rule;
	}
}
