package de.hetzge.tilemapgen.map.impl;

import de.hetzge.tilemapgen.base.def.Position;
import de.hetzge.tilemapgen.base.def.Size;
import de.hetzge.tilemapgen.base.impl.SizePositions;
import de.hetzge.tilemapgen.map.def.Layer;
import de.hetzge.tilemapgen.map.def.Zone;
import javaslang.Lazy;

public final class CachedLayer implements Layer {

	private final Lazy<Layer> cachedLayerLazy;

	public CachedLayer(Layer sourceLayer) {
		this.cachedLayerLazy = Lazy.of(() -> {
			final SimpleLayer layer = new SimpleLayer(sourceLayer.zone(), sourceLayer.size());
			for (Position position : new SizePositions(sourceLayer.size())) {
				layer.set(position, sourceLayer.is(position));
			}
			return layer;
		});
	}

	@Override
	public Zone zone() {
		return this.cachedLayerLazy.get().zone();
	}

	@Override
	public Size size() {
		return this.cachedLayerLazy.get().size();
	}

	@Override
	public boolean is(Position position) {
		return this.cachedLayerLazy.get().is(position);
	}
}
