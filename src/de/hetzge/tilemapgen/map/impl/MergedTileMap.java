package de.hetzge.tilemapgen.map.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import de.hetzge.tilemapgen.base.def.Position;
import de.hetzge.tilemapgen.base.impl.SizePositions;
import de.hetzge.tilemapgen.map.def.Layer;
import de.hetzge.tilemapgen.map.def.TileMap;
import de.hetzge.tilemapgen.map.def.Zone;

public final class MergedTileMap implements TileMap {

	private final TileMap tileMap;
	private final TileMap otherTileMap;

	public MergedTileMap(TileMap tileMap, TileMap otherTileMap) {
		this.tileMap = tileMap;
		this.otherTileMap = otherTileMap;
	}

	@Override
	public List<Layer> layers() {
		final List<Layer> newLayers = new ArrayList<>();
		final List<Layer> layers = new CombinedTileMap(this.tileMap, this.otherTileMap).layers();
		final List<Zone> zones = layers.stream().map(Layer::zone).collect(Collectors.toList());
		final Map<Zone, List<Layer>> layersByZone = layers.stream().collect(Collectors.groupingBy(Layer::zone));

		for (Zone zone : zones) {
			final SimpleLayer newLayer = new SimpleLayer(zone, size());
			for (Position position : new SizePositions(size())) {
				newLayer.set(position, layersByZone.get(zone).stream().anyMatch(layer -> layer.is(position)));
			}
			newLayers.add(newLayer);
		}

		return newLayers;
	}

}
