package de.hetzge.tilemapgen.map.impl;

import java.util.Objects;

import de.hetzge.tilemapgen.map.def.Zone;

public final class SimpleZone implements Zone {

	private final String key;

	public SimpleZone(String key) {
		this.key = key;
	}

	@Override
	public String key() {
		return this.key;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.key);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof SimpleZone)) {
			return false;
		}
		final SimpleZone other = (SimpleZone) obj;
		return Objects.equals(this.key, other.key);
	}

	@Override
	public String toString() {
		return "SimpleZone [name=" + this.key + "]";
	}
}
