package de.hetzge.tilemapgen.map.impl;

import de.hetzge.tilemapgen.base.def.Percent;
import de.hetzge.tilemapgen.map.def.Zone;

public class ZoneTransitionConfiguration implements Comparable<ZoneTransitionConfiguration> {
	private final Percent minValue;
	private final Zone zone;

	public ZoneTransitionConfiguration(Percent minValue, Zone zone) {
		this.minValue = minValue;
		this.zone = zone;
	}

	public boolean match(Percent percent) {
		return this.minValue.value() <= percent.value();
	}

	public Zone zone() {
		return this.zone;
	}

	@Override
	public int compareTo(ZoneTransitionConfiguration other) {
		return new Percent.DefaultComparator().compare(other.minValue, this.minValue);
	}
}