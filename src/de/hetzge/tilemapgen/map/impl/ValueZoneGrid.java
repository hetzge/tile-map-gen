package de.hetzge.tilemapgen.map.impl;

import de.hetzge.tilemapgen.base.def.Grid;
import de.hetzge.tilemapgen.base.def.Percent;
import de.hetzge.tilemapgen.base.def.Position;
import de.hetzge.tilemapgen.base.def.Size;
import de.hetzge.tilemapgen.map.def.Zone;
import de.hetzge.tilemapgen.map.def.ZoneTransitions;
import javaslang.control.Option;

public final class ValueZoneGrid implements Grid<Zone> {

	private final Grid<Percent> valueGrid;
	private final ZoneTransitions zoneTransitions;

	public ValueZoneGrid(Grid<Percent> valueGrid, ZoneTransitions zoneTransitions) {
		this.valueGrid = valueGrid;
		this.zoneTransitions = zoneTransitions;
	}

	@Override
	public Size size() {
		return this.valueGrid.size();
	}

	@Override
	public Option<Zone> get(Position position) {
		return this.valueGrid.get(position).flatMap(this.zoneTransitions::zone);
	}

}
