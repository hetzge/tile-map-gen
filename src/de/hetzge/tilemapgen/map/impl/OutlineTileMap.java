package de.hetzge.tilemapgen.map.impl;

import java.util.List;
import java.util.stream.Collectors;

import de.hetzge.tilemapgen.map.def.Layer;
import de.hetzge.tilemapgen.map.def.TileMap;

public final class OutlineTileMap implements TileMap {

	private final TileMap tileMap;

	public OutlineTileMap(TileMap tileMap) {
		this.tileMap = tileMap;
	}

	@Override
	public List<Layer> layers() {
		return this.tileMap.layers().stream().map(OutlineLayer::new).collect(Collectors.toList());
	}
}
