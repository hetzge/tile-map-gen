package de.hetzge.tilemapgen.map.impl;

import de.hetzge.tilemapgen.base.def.Percent;
import de.hetzge.tilemapgen.map.def.Zone;
import de.hetzge.tilemapgen.map.def.ZoneTransitions;
import javaslang.collection.List;
import javaslang.control.Option;

/**
 * Configured distribution of zones
 */
public final class ConfiguredZoneTransitions implements ZoneTransitions {

	private final List<ZoneTransitionConfiguration> configurations;

	public ConfiguredZoneTransitions() {
		this(List.empty());
	}

	public ConfiguredZoneTransitions(List<ZoneTransitionConfiguration> configurations) {
		this.configurations = configurations.sorted();
	}

	@Override
	public Option<Zone> zone(Percent value) {
		return this.configurations.filter(it -> it.match(value)).headOption().map(ZoneTransitionConfiguration::zone);
	}

	public ConfiguredZoneTransitions with(Percent minValue, Zone zone) {
		return new ConfiguredZoneTransitions(this.configurations.append(new ZoneTransitionConfiguration(minValue, zone)));
	}
}
