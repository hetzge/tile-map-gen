package de.hetzge.tilemapgen.map.impl;

import java.util.List;

import de.hetzge.tilemapgen.map.def.Layer;
import de.hetzge.tilemapgen.map.def.TileMap;

public final class SimpleTileMap implements TileMap {

	private final List<Layer> layers;

	public SimpleTileMap(List<Layer> layers) {
		this.layers = layers;
	}

	@Override
	public List<Layer> layers() {
		return this.layers;
	}
}
