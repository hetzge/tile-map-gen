package de.hetzge.tilemapgen.map.impl;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Base64;
import java.util.Collection;
import java.util.stream.Collectors;

import de.hetzge.tilemapgen.base.def.Format;
import de.hetzge.tilemapgen.base.def.Size;
import de.hetzge.tilemapgen.map.def.Layer;
import de.hetzge.tilemapgen.map.def.TileMap;

public final class TileMapJsonFormat implements Format<TileMap> {

	@Override
	public void write(OutputStream out, TileMap tileMap) throws IOException {
		final Collection<Layer> layers = tileMap.layers();
		final Size size = tileMap.size();
		try (PrintWriter writer = new PrintWriter(out)) {
			writer.append("{");
			writer.append("\"width\":").append(Integer.toString(size.width())).append(",");
			writer.append("\"height\":").append(Integer.toString(size.height())).append(",");
			writer.append("\"persons\":[").append(person()).append("],");
			writer.append("\"layers\":[");
			writer.append(layers.stream().map(this::layer).collect(Collectors.joining(",")));
			writer.append("]");
			writer.append("}");
		}
	}
	
	private CharSequence person() {
		final StringBuilder builder = new StringBuilder();
		builder.append("{");
		builder.append("\"type\":\"StartPersonType\",");
		builder.append("\"x\":50,");
		builder.append("\"y\":50,");
		builder.append("\"playerId\":1");
		builder.append("}");
		return builder;
	}

	private CharSequence layer(Layer layer) {
		final StringBuilder builder = new StringBuilder();
		builder.append("{");
		builder.append("\"name\":\"").append(layer.zone().key()).append("\",");
		builder.append("\"data\":\"").append(layerData(layer)).append("\"");
		builder.append("}");
		return builder;
	}

	private String layerData(Layer layer) {
		return escape(new String(Base64.getEncoder().encode(layer.toBitSet().toByteArray())));
	}

	private String escape(String raw) {
		String escaped = raw;
		escaped = escaped.replace("\\", "\\\\");
		escaped = escaped.replace("\"", "\\\"");
		escaped = escaped.replace("\b", "\\b");
		escaped = escaped.replace("\f", "\\f");
		escaped = escaped.replace("\n", "\\n");
		escaped = escaped.replace("\r", "\\r");
		escaped = escaped.replace("\t", "\\t");
		return escaped;
	}
}
