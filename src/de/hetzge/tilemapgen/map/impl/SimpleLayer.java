package de.hetzge.tilemapgen.map.impl;

import java.util.BitSet;

import de.hetzge.tilemapgen.base.def.Position;
import de.hetzge.tilemapgen.base.def.Size;
import de.hetzge.tilemapgen.base.impl.PositionIndex;
import de.hetzge.tilemapgen.map.def.Layer;
import de.hetzge.tilemapgen.map.def.Zone;

public final class SimpleLayer implements Layer {

	private final Zone zone;
	private final Size size;
	private final BitSet values;

	public SimpleLayer(Zone zone, Size size) {
		this(zone, size, new BitSet(size.area()));
	}

	public SimpleLayer(Zone zone, Size size, BitSet values) {
		this.zone = zone;
		this.size = size;
		this.values = values;
	}

	@Override
	public Zone zone() {
		return this.zone;
	}

	@Override
	public Size size() {
		return this.size;
	}
	
	@Override
	public boolean is(Position position) {
		return this.values.get(index(position));
	}

	public void set(Position position, boolean is) {
		this.values.set(index(position), is);
	}

	private int index(Position position) {
		return new PositionIndex(position, this.size).value();
	}
}
