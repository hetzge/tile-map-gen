package de.hetzge.tilemapgen.map.impl;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

import de.hetzge.tilemapgen.base.def.Format;
import de.hetzge.tilemapgen.map.def.Layer;
import de.hetzge.tilemapgen.map.def.TileMap;

public class TileMapDebugFormat implements Format<TileMap> {

	@Override
	public void write(OutputStream out, TileMap tileMap) throws IOException {
		try (PrintWriter writer = new PrintWriter(out)) {
			for (Layer layer : tileMap.layers()) {
				writer.println("Layer: " + layer.zone().key());
				writer.println(layer.toDebugString());
			}
		}
	}

}
