package de.hetzge.tilemapgen.map.impl;

import java.util.Random;

import de.hetzge.tilemapgen.map.def.Rule;
import de.hetzge.tilemapgen.map.def.Zone;
import javaslang.collection.HashMap;
import javaslang.collection.Map;

public final class ProbabilityRule implements Rule {

	private final Random random;
	private final Map<Zone, Map<Zone, Float>> probabilityByOnTopOfZoneByZone;

	public ProbabilityRule(Random random) {
		this(random, HashMap.empty());
	}

	private ProbabilityRule(Random random, Map<Zone, Map<Zone, Float>> probabilityByOnTopOfZoneByZone) {
		this.random = random;
		this.probabilityByOnTopOfZoneByZone = probabilityByOnTopOfZoneByZone;
	}

	@Override
	public boolean allowed(Zone zone, Zone onTopOf) {
		final float probability = this.probabilityByOnTopOfZoneByZone.get(zone).getOrElse(HashMap.empty()).get(onTopOf).getOrElse(1.0f);
		return probability >= this.random.nextFloat();
	}

	public ProbabilityRule with(Zone zone, Zone onTopOf, float probability) {
		final Map<Zone, Float> innerMap = this.probabilityByOnTopOfZoneByZone.get(zone).getOrElse(HashMap.empty());
		return new ProbabilityRule(this.random, this.probabilityByOnTopOfZoneByZone.put(zone, innerMap.put(onTopOf, probability)));
	}
}
