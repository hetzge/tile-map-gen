package de.hetzge.tilemapgen.map.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import de.hetzge.tilemapgen.base.def.Grid;
import de.hetzge.tilemapgen.base.def.Position;
import de.hetzge.tilemapgen.base.impl.SizePositions;
import de.hetzge.tilemapgen.base.impl.Utils;
import de.hetzge.tilemapgen.map.def.Layer;
import de.hetzge.tilemapgen.map.def.TileMap;
import de.hetzge.tilemapgen.map.def.Zone;
import javaslang.control.Option;

public final class SeparateHierarchicalZonesTileMap implements TileMap {

	private final List<Zone> zones;
	private final Grid<Zone> zoneGrid;
	private final Comparator<Zone> comparator;

	public SeparateHierarchicalZonesTileMap(List<Zone> zones, Grid<Zone> zoneGrid, Comparator<Zone> comparator) {
		this.zones = zones;
		this.zoneGrid = zoneGrid;
		this.comparator = comparator;
	}

	@Override
	public List<Layer> layers() {
		final Map<Zone, SimpleLayer> layersByZone = this.zones.stream().collect(Utils.toLinkedMap(zone -> zone, zone -> new SimpleLayer(zone, this.zoneGrid.size())));

		final List<Position> positions = new SizePositions(this.zoneGrid.size()).toList();
		Collections.shuffle(positions, new Random(12345));

		for (Zone zone : layersByZone.keySet()) {
			for (Position position : positions) {
				for (Zone currentZone : this.zoneGrid.get(position)) {
					for (SimpleLayer layer : Option.of(layersByZone.get(zone))) {
						final int compareValue = this.comparator.compare(currentZone, zone);
						if (compareValue >= 0) {
							layer.set(position, true);
						}
					}
				}
			}
		}

		return new ArrayList<>(layersByZone.values());
	}

}
