package de.hetzge.tilemapgen.map.impl;

import de.hetzge.tilemapgen.base.def.Position;
import de.hetzge.tilemapgen.base.def.Size;
import de.hetzge.tilemapgen.base.impl.AroundPositions;
import de.hetzge.tilemapgen.map.def.Layer;
import de.hetzge.tilemapgen.map.def.Zone;
import javaslang.collection.Iterator;

public final class IncreaseZoneLayer implements Layer {

	private final Layer originalLayer;

	public IncreaseZoneLayer(Layer originalLayer) {
		this.originalLayer = originalLayer;
	}

	@Override
	public Zone zone() {
		return this.originalLayer.zone();
	}

	@Override
	public Size size() {
		return this.originalLayer.size();
	}

	@Override
	public boolean is(Position position) {
		return this.originalLayer.is(position) || Iterator.ofAll(new AroundPositions(position, size())).exists(this.originalLayer::is);
	}
}
