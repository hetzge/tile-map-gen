package de.hetzge.tilemapgen.map.impl;

import java.util.List;

import de.hetzge.tilemapgen.base.impl.Utils;
import de.hetzge.tilemapgen.map.def.Layer;
import de.hetzge.tilemapgen.map.def.TileMap;

public final class CombinedTileMap implements TileMap {

	private final TileMap tileMap;
	private final TileMap otherTileMap;

	public CombinedTileMap(TileMap tileMap, TileMap otherTileMap) {
		this.tileMap = tileMap;
		this.otherTileMap = otherTileMap;
	}

	@Override
	public List<Layer> layers() {
		return Utils.concat(this.tileMap.layers(), this.otherTileMap.layers());
	}

}
