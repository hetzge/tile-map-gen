package de.hetzge.tilemapgen.map.def;

import java.util.BitSet;

import de.hetzge.tilemapgen.base.def.Position;
import de.hetzge.tilemapgen.base.def.Size;
import de.hetzge.tilemapgen.base.impl.PositionIndex;
import de.hetzge.tilemapgen.base.impl.SimplePosition;
import de.hetzge.tilemapgen.base.impl.SizePositions;

public interface Layer {
	Size size();

	Zone zone();

	boolean is(Position position);

	default BitSet toBitSet() {
		final Size size = size();
		final BitSet bitSet = new BitSet(size.area());
		for (Position position : new SizePositions(size)) {
			bitSet.set(new PositionIndex(position, size).value(), is(position));
		}
		return bitSet;
	}

	default CharSequence toDebugString() {
		final StringBuilder builder = new StringBuilder();
		for (int y = 0; y < size().height(); y++) {
			for (int x = 0; x < size().width(); x++) {
				builder.append(is(new SimplePosition(x, y)) ? "X" : "_");
			}
			builder.append("\n");
		}
		return builder;
	}
}