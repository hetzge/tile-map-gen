package de.hetzge.tilemapgen.map.def;

import de.hetzge.tilemapgen.base.def.Percent;
import javaslang.control.Option;

public interface ZoneTransitions {
	Option<Zone> zone(Percent value);
}
