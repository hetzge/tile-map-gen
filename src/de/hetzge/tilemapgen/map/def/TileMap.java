package de.hetzge.tilemapgen.map.def;

import java.util.List;

import de.hetzge.tilemapgen.base.def.Size;
import de.hetzge.tilemapgen.base.impl.SimpleSize;

public interface TileMap {

	List<Layer> layers();

	default Size size() {
		return layers().stream().findFirst().map(Layer::size).orElseGet(() -> new SimpleSize(0));
	}

	default CharSequence toDebugString() {
		final StringBuilder builder = new StringBuilder();
		for (Layer layer : layers()) {
			builder.append("--- LAYER (");
			builder.append(layer.zone().key());
			builder.append(") ---\n");
			builder.append(layer.toDebugString());
		}
		return builder;
	}
}