package de.hetzge.tilemapgen.map.def;

public interface Rule {
	boolean allowed(Zone zone, Zone onTopOf);
}
