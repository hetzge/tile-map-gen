package de.hetzge.tilemapgen;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

import de.hetzge.tilemapgen.base.def.Grid;
import de.hetzge.tilemapgen.base.def.Size;
import de.hetzge.tilemapgen.base.impl.MaskGrid;
import de.hetzge.tilemapgen.base.impl.MergedGrid;
import de.hetzge.tilemapgen.base.impl.RadiusPositionProbability;
import de.hetzge.tilemapgen.base.impl.SimplePercent;
import de.hetzge.tilemapgen.base.impl.SimplePosition;
import de.hetzge.tilemapgen.base.impl.SimpleSize;
import de.hetzge.tilemapgen.generator.impl.BorderValueChunkGrid;
import de.hetzge.tilemapgen.generator.impl.ChunkGrid;
import de.hetzge.tilemapgen.generator.impl.ChunkedValueGrid;
import de.hetzge.tilemapgen.map.def.Layer;
import de.hetzge.tilemapgen.map.def.TileMap;
import de.hetzge.tilemapgen.map.def.Zone;
import de.hetzge.tilemapgen.map.def.ZoneTransitions;
import de.hetzge.tilemapgen.map.impl.AccurateZoneTransitions;
import de.hetzge.tilemapgen.map.impl.CombinedTileMap;
import de.hetzge.tilemapgen.map.impl.ConfiguredZoneTransitions;
import de.hetzge.tilemapgen.map.impl.DisallowRule;
import de.hetzge.tilemapgen.map.impl.IncreaseZoneTileMap;
import de.hetzge.tilemapgen.map.impl.OutlineTileMap;
import de.hetzge.tilemapgen.map.impl.ProbabilityRule;
import de.hetzge.tilemapgen.map.impl.RuledTileMap;
import de.hetzge.tilemapgen.map.impl.SeparateHierarchicalZonesTileMap;
import de.hetzge.tilemapgen.map.impl.SeparateZonesTileMap;
import de.hetzge.tilemapgen.map.impl.SimpleZone;
import de.hetzge.tilemapgen.map.impl.ValueZoneGrid;
import javaslang.Lazy;

public final class SGameTileMap implements TileMap {

	private final Zone GRASS_ZONE = new SimpleZone("GRASS");
	private final Zone DESERT_ZONE = new SimpleZone("DESERT");
	private final Zone WATER_ZONE = new SimpleZone("WATER");

	private final Zone FOREST_ZONE = new SimpleZone("FOREST");
	private final Zone STONE_ZONE = new SimpleZone("STONE");

	private final Size size;
	private final Random random;

	private final Lazy<TileMap> tileMapLazy;

	public SGameTileMap(Size size, Random random) {
		this.size = size;
		this.random = random;
		this.tileMapLazy = Lazy.of(this::generate);
	}

	@Override
	public List<Layer> layers() {
		return this.tileMapLazy.get().layers();
	}

	private TileMap generate() {
		return new RuledTileMap(new CombinedTileMap(new CombinedTileMap(generateGroundTileMap(), generateMountainTileMap()), generateEnvironmentTileMap()), Arrays.asList(new DisallowRule().with(javaslang.collection.List.ofAll(MountainZone.zones()), this.FOREST_ZONE, this.STONE_ZONE), new DisallowRule().with(this.WATER_ZONE, this.FOREST_ZONE, this.STONE_ZONE), new ProbabilityRule(this.random).with(this.FOREST_ZONE, this.GRASS_ZONE, 0.8f),
				new ProbabilityRule(this.random).with(this.FOREST_ZONE, this.DESERT_ZONE, 0.1f), new ProbabilityRule(this.random).with(this.STONE_ZONE, this.DESERT_ZONE, 0.9f), new ProbabilityRule(this.random).with(this.STONE_ZONE, this.GRASS_ZONE, 0.2f)));
	}

	private TileMap generateGroundTileMap() {
		final Grid<Zone> groundZoneGrid = generateGroundZoneGrid();
		final Grid<Zone> desertZoneGrid = new MaskGrid<>(generateDesertZoneGrid(), new RadiusPositionProbability(new SimplePosition(100, 100), 75, 50));
		return new IncreaseZoneTileMap(new SeparateZonesTileMap(Arrays.asList(this.GRASS_ZONE, this.DESERT_ZONE, this.WATER_ZONE), new MergedGrid<>(groundZoneGrid, desertZoneGrid)));
	}

	private TileMap generateMountainTileMap() {
		final List<Zone> zones = Arrays.asList(MountainZone.values());
		final ZoneTransitions zoneTransitions = new AccurateZoneTransitions(zones);

		final Size chunkSize = new SimpleSize(25);
		final Grid<Zone> zoneGrid = new ValueZoneGrid(new ChunkedValueGrid(this.size, chunkSize, new ChunkGrid(new BorderValueChunkGrid(this.size.divide(chunkSize), new SimplePercent(0f), this.random), this.random)), zoneTransitions);
		final TileMap seperatedZoneTileMap = new SeparateHierarchicalZonesTileMap(zones, zoneGrid, MountainZoneComparator.INSTANCE);
		return new OutlineTileMap(new IncreaseZoneTileMap(seperatedZoneTileMap));
	}

	private TileMap generateEnvironmentTileMap() {
		final Grid<Zone> environmentZoneGrid = generateEnvironmentZoneGrid();
		return new SeparateZonesTileMap(Arrays.asList(this.FOREST_ZONE, this.STONE_ZONE), environmentZoneGrid);
	}

	private Grid<Zone> generateGroundZoneGrid() {
		final ZoneTransitions zoneTransitions = new ConfiguredZoneTransitions().with(new SimplePercent(0.0f), this.GRASS_ZONE).with(new SimplePercent(0.75f), this.DESERT_ZONE).with(new SimplePercent(0.8f), this.WATER_ZONE);
		return new ValueZoneGrid(new ChunkedValueGrid(this.size, new SimpleSize(50), this.random), zoneTransitions);
	}

	private Grid<Zone> generateDesertZoneGrid() {
		final ZoneTransitions zoneTransitions = new ConfiguredZoneTransitions().with(new SimplePercent(0.2f), this.DESERT_ZONE).with(new SimplePercent(0.95f), this.WATER_ZONE);
		return new ValueZoneGrid(new ChunkedValueGrid(this.size, new SimpleSize(10), this.random), zoneTransitions);
	}

	private Grid<Zone> generateEnvironmentZoneGrid() {
		final Grid<Zone> forestZoneGrid = new ValueZoneGrid(new ChunkedValueGrid(this.size, new SimpleSize(25), this.random), new ConfiguredZoneTransitions().with(new SimplePercent(0.5f), this.FOREST_ZONE));
		final Grid<Zone> stoneZoneGrid = new ValueZoneGrid(new ChunkedValueGrid(this.size, new SimpleSize(10), this.random), new ConfiguredZoneTransitions().with(new SimplePercent(0.75f), this.STONE_ZONE));
		return new MergedGrid<>(stoneZoneGrid, forestZoneGrid);
	}

	private enum MountainZone implements Zone {
		ONE, TWO, THREE, FOUR, FIVE;

		@Override
		public String key() {
			return "MOUNTAIN_" + (ordinal() + 1);
		}

		public static List<Zone> zones() {
			return Arrays.asList(values());
		}
	}

	private enum MountainZoneComparator implements Comparator<Zone> {
		INSTANCE;

		@Override
		public int compare(Zone a, Zone b) {
			if (a instanceof MountainZone && b instanceof MountainZone) {
				final MountainZone mountainZoneA = (MountainZone) a;
				final MountainZone mountainZoneB = (MountainZone) b;
				return mountainZoneA.compareTo(mountainZoneB);
			} else {
				throw new IllegalStateException();
			}
		}
	}
}