package de.hetzge.tilemapgen.base.impl;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import de.hetzge.tilemapgen.base.def.Position;

public abstract class BasePosition implements Position {

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(this.x()).append(this.y()).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Position)) {
			return false;
		}
		Position other = (Position) obj;
		return new EqualsBuilder().append(this.x(), other.x()).append(this.y(), other.y()).isEquals();
	}

	@Override
	public String toString() {
		return "BasePosition [x=" + this.x() + ", y=" + this.y() + "]";
	}
}
