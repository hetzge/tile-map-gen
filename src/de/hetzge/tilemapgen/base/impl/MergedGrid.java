package de.hetzge.tilemapgen.base.impl;

import de.hetzge.tilemapgen.base.def.Grid;
import de.hetzge.tilemapgen.base.def.Position;
import de.hetzge.tilemapgen.base.def.Size;
import javaslang.control.Option;

public class MergedGrid<T> implements Grid<T> {

	private final Grid<T> grid;
	private final Grid<T> otherGrid;

	public MergedGrid(Grid<T> grid, Grid<T> otherGrid) {
		this.grid = grid;
		this.otherGrid = otherGrid;
	}

	@Override
	public Size size() {
		return this.grid.size();
	}

	@Override
	public Option<T> get(Position position) {
		return this.otherGrid.get(position).orElse(() -> this.grid.get(position));
	}
}
