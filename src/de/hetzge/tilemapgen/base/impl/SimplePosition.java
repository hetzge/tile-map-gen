package de.hetzge.tilemapgen.base.impl;

public final class SimplePosition extends BasePosition {

	private final int x;
	private final int y;

	public SimplePosition(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public int x() {
		return this.x;
	}

	@Override
	public int y() {
		return this.y;
	}

}
