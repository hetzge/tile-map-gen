package de.hetzge.tilemapgen.base.impl;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import de.hetzge.tilemapgen.base.def.Size;

public abstract class BaseSize implements Size {

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(this.width()).append(this.height()).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (Size.class.isInstance(obj)) {
			return false;
		}
		Size other = (Size) obj;
		return new EqualsBuilder().append(this.width(), other.width()).append(this.height(), other.height()).isEquals();
	}

	@Override
	public String toString() {
		return "BaseSize [width=" + this.width() + ", height=" + this.height() + "]";
	}

}
