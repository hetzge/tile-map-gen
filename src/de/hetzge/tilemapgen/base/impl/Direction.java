package de.hetzge.tilemapgen.base.impl;

public enum Direction {

	TOP_LEFT(-1, -1), TOP(0, -1), TOP_RIGHT(1, -1), LEFT(-1, 0), CENTER(0, 0), RIGHT(1, 0), BOTTOM_LEFT(-1, 1), BOTTOM(0, 1), BOTTOM_RIGHT(1, 1);

	public static final Direction[] VALUES = values();

	private final int x;
	private final int y;

	private Direction(int x, int y) {
		if (x > 1 || x < -1 || y > 1 || y < -1) {
			throw new IllegalArgumentException(String.format("Invalid direction values %s/%s", x, y));
		}

		this.x = x;
		this.y = y;
	}

	public int x() {
		return this.x;
	}

	public int y() {
		return this.y;
	}
}
