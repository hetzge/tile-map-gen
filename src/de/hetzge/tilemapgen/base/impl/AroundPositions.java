package de.hetzge.tilemapgen.base.impl;

import java.util.Iterator;

import de.hetzge.tilemapgen.base.def.Position;
import de.hetzge.tilemapgen.base.def.Size;

public class AroundPositions implements Iterable<Position> {

	private final Position position;
	private final Size size;

	public AroundPositions(Position position, Size size) {
		this.position = position;
		this.size = size;
	}

	@Override
	public Iterator<Position> iterator() {
		final int x = this.position.x();
		final int y = this.position.y();
		
		return javaslang.collection.Iterator.of(new SimplePosition(x + 1, y), new SimplePosition(x - 1, y), new SimplePosition(x, y + 1), new SimplePosition(x, y - 1)).filter(this::isValid).map(Position.class::cast);
	}

	private boolean isValid(Position position) {
		return position.x() >= 0 && position.x() < this.size.width() && position.y() >= 0 && position.y() < this.size.height();
	}
}
