package de.hetzge.tilemapgen.base.impl;

import de.hetzge.tilemapgen.base.def.Percent;
import de.hetzge.tilemapgen.base.def.Position;
import de.hetzge.tilemapgen.base.def.Probability;

public class RadiusPositionProbability implements Probability<Position> {

	private final Position rootPosition;
	private final int radius;
	private final int startTransitionRadius;

	public RadiusPositionProbability(Position rootPosition, int radius, int startTransitionRadius) {
		this.rootPosition = rootPosition;
		this.radius = radius;
		this.startTransitionRadius = startTransitionRadius;
	}

	@Override
	public Percent value(Position position) {
		final int distanceToRoot = position.distance(this.rootPosition);
		if (distanceToRoot > this.radius) {
			return new SimplePercent(0f);
		} else if (distanceToRoot < this.startTransitionRadius) {
			return new SimplePercent(1f);
		} else {
			final int transitionZoneDistance = this.radius - this.startTransitionRadius;
			final int transitionDistance = distanceToRoot - this.startTransitionRadius;
			return new SimplePercent(1f - (float) transitionDistance / (float) transitionZoneDistance);
		}
	}
}
