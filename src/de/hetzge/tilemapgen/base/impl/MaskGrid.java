package de.hetzge.tilemapgen.base.impl;

import java.util.Random;

import de.hetzge.tilemapgen.base.def.Grid;
import de.hetzge.tilemapgen.base.def.Position;
import de.hetzge.tilemapgen.base.def.Probability;
import de.hetzge.tilemapgen.base.def.Size;
import javaslang.control.Option;

public class MaskGrid<T> implements Grid<T> {

	private final Grid<T> grid;
	private final Probability<Position> positionProbability;
	private final Random random;

	public MaskGrid(Grid<T> grid, Probability<Position> positionProbability) {
		this(grid, positionProbability, new Random(0));
	}

	public MaskGrid(Grid<T> grid, Probability<Position> positionProbability, Random random) {
		this.grid = grid;
		this.positionProbability = positionProbability;
		this.random = random;
	}

	@Override
	public Size size() {
		return this.grid.size();
	}

	@Override
	public Option<T> get(Position position) {
		if (this.positionProbability.is(position, this.random)) {
			return this.grid.get(position);
		} else {
			return Option.none();
		}
	}

}
