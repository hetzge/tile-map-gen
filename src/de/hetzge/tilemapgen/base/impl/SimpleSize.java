package de.hetzge.tilemapgen.base.impl;

public final class SimpleSize extends BaseSize {

	private final int width;
	private final int height;

	public SimpleSize(int size) {
		this.width = size;
		this.height = size;
	}

	public SimpleSize(int width, int height) {
		this.width = width;
		this.height = height;
	}

	@Override
	public int width() {
		return this.width;
	}

	@Override
	public int height() {
		return this.height;
	}

}
