package de.hetzge.tilemapgen.base.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javaslang.control.Option;

public final class Utils {

	private Utils() {
	}

	/**
	 * Creates a {@link ArrayList} full of <code>null</code>s
	 */
	public static final <T> List<T> nullArrayList(int size) {
		return nullList(new ArrayList<>(), size);
	}

	/**
	 * Creates a {@link LinkedList} full of <code>null</code>s
	 */
	public static final <T> List<T> nullLinkedList(int size) {
		return nullList(new LinkedList<>(), size);
	}

	/**
	 * Fills a {@link List} full of <code>null</code>s
	 */
	private static final <T> List<T> nullList(List<T> list, int size) {
		for (int i = 0; i < size; i++) {
			list.add(null);
		}
		return list;
	}

	/**
	 * Collects a stream to a {@link LinkedHashMap}
	 * 
	 * @see http://stackoverflow.com/a/29090335/7662651
	 */
	public static <T, K, U> Collector<T, ?, Map<K, U>> toLinkedMap(Function<? super T, ? extends K> keyMapper, Function<? super T, ? extends U> valueMapper) {
		return Collectors.toMap(keyMapper, valueMapper, (u, v) -> {
			throw new IllegalStateException(String.format("Duplicate key %s", u));
		}, LinkedHashMap::new);
	}

	public static <T> List<T> toList(Iterable<T> iterable) {
		return toList(iterable.iterator());
	}

	public static <T> List<T> toList(Iterator<T> iterator) {
		final List<T> list = new ArrayList<>();
		iterator.forEachRemaining(list::add);
		return list;
	}

	@SafeVarargs
	public static <T> List<T> concat(List<T>... lists) {
		return Arrays.asList(lists).stream().collect(Collectors.reducing((a, b) -> Stream.concat(a.stream(), b.stream()).collect(Collectors.toList()))).orElse(Collections.emptyList());
	}

	public static <T> List<T> reverse(List<T> list) {
		final List<T> reversedList = new ArrayList<>(list);
		Collections.reverse(reversedList);
		return reversedList;
	}

	public static <T> Option<T> head(List<T> list) {
		return !list.isEmpty() ? Option.of(list.get(0)) : Option.none();
	}

	public static <T> Option<List<T>> tail(List<T> list) {
		return list.size() > 1 ? Option.of(list.subList(1, list.size())) : Option.none();
	}
}
