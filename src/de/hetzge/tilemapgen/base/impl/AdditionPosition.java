package de.hetzge.tilemapgen.base.impl;

import de.hetzge.tilemapgen.base.def.Position;

public class AdditionPosition extends BasePosition {

	private final Position source;
	private final Position target;

	public AdditionPosition(Position source, Position target) {
		this.source = source;
		this.target = target;
	}

	@Override
	public int x() {
		return this.source.x() + this.target.x();
	}

	@Override
	public int y() {
		return this.source.y() + this.target.y();
	}

}
