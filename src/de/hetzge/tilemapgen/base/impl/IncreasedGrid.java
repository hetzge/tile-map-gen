package de.hetzge.tilemapgen.base.impl;

import de.hetzge.tilemapgen.base.def.Grid;
import de.hetzge.tilemapgen.base.def.Position;
import de.hetzge.tilemapgen.base.def.Size;
import javaslang.collection.Iterator;
import javaslang.control.Option;

public class IncreasedGrid<T> implements Grid<T> {

	private final Grid<T> grid;

	public IncreasedGrid(Grid<T> grid) {
		this.grid = grid;
	}

	@Override
	public Size size() {
		return this.grid.size();
	}

	@Override
	public Option<T> get(Position position) {
		return this.grid.get(position).orElse(() -> Iterator.ofAll(new AroundPositions(position, size())).map(this.grid::get).head());
	}
}
