package de.hetzge.tilemapgen.base.impl;

import de.hetzge.tilemapgen.base.def.Position;

public class AroundPosition extends BasePosition {

	private final Position position;
	private final Direction direction;

	public AroundPosition(Position position, Direction direction) {
		this.position = position;
		this.direction = direction;
	}

	@Override
	public int x() {
		return this.position.x() + this.direction.x();
	}

	@Override
	public int y() {
		return this.position.y() + this.direction.y();
	}

}
