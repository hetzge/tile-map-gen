package de.hetzge.tilemapgen.base.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import de.hetzge.tilemapgen.base.def.Grid;
import de.hetzge.tilemapgen.base.def.MutableGrid;
import de.hetzge.tilemapgen.base.def.Position;
import de.hetzge.tilemapgen.base.def.Size;
import javaslang.control.Option;

public final class SimpleMutableGrid<T> implements MutableGrid<T> {

	private final Size size;
	private final List<T> cells;

	/**
	 * Create a new mutable grid with given <code>size</code>.
	 */
	public SimpleMutableGrid(Size size) {
		this(size, Utils.nullArrayList((size.width() * size.height())));
	}

	/**
	 * Convert a existing grid to a mutable one.
	 */
	public SimpleMutableGrid(Grid<T> grid) {
		this(grid.size(), new ArrayList<>(Utils.toList(new SizePositions(grid.size())).stream().map(grid::get).map(it -> it.isDefined() ? it.get() : null).collect(Collectors.toList())));
	}

	private SimpleMutableGrid(Size size, List<T> cells) {
		if (size.area() != cells.size()) {
			throw new IllegalArgumentException(String.format("Cells size of %s does not match size of %s", cells.size(), size.area()));
		}

		this.size = size;
		this.cells = cells;
	}

	@Override
	public Size size() {
		return this.size;
	}

	@Override
	public Option<T> get(Position position) {
		final int index = new PositionIndex(position, this.size).value();
		return isValid(index) ? Option.of(this.cells.get(index)) : Option.none();
	}

	@Override
	public void set(Position position, T value) {
		final int index = new PositionIndex(position, this.size).value();
		if (isValid(index)) {
			this.cells.set(index, value);
		}
	}

	private boolean isValid(int index) {
		return index >= 0 && index < this.size.area();
	}
}
