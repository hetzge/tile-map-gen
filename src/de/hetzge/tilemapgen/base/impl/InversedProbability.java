package de.hetzge.tilemapgen.base.impl;

import de.hetzge.tilemapgen.base.def.Percent;
import de.hetzge.tilemapgen.base.def.Probability;

public class InversedProbability<T> implements Probability<T> {

	private final Probability<T> probability;

	public InversedProbability(Probability<T> probability) {
		this.probability = probability;
	}

	@Override
	public Percent value(T t) {
		return this.probability.value(t).inverse();
	}
}
