package de.hetzge.tilemapgen.base.impl;

import de.hetzge.tilemapgen.base.def.Index;
import de.hetzge.tilemapgen.base.def.Position;
import de.hetzge.tilemapgen.base.def.Size;

public final class PositionIndex implements Index {

	private final Position position;
	private final Size size;

	public PositionIndex(Position position, Size size) {
		this.position = position;
		this.size = size;
	}

	@Override
	public int value() {
		return this.position.y() * this.size.width() + this.position.x();
	}
}
