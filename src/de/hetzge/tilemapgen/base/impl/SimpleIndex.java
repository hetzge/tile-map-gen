package de.hetzge.tilemapgen.base.impl;

import de.hetzge.tilemapgen.base.def.Index;

public final class SimpleIndex implements Index {

	private final int value;

	public SimpleIndex(int value) {
		this.value = value;
	}

	@Override
	public int value() {
		return this.value;
	}
}
