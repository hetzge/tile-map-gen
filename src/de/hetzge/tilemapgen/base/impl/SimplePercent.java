package de.hetzge.tilemapgen.base.impl;

import java.text.MessageFormat;
import java.util.Objects;

import de.hetzge.tilemapgen.base.def.Percent;

public class SimplePercent implements Percent {

	private final float value;

	public SimplePercent(float value) {
		if (value < 0f || value > 1f) {
			throw new IllegalArgumentException(MessageFormat.format("Illegal percentage value ''{0}''", value));
		}

		this.value = value;
	}

	@Override
	public float value() {
		return this.value;
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.value);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Percent)) {
			return false;
		}
		Percent other = (Percent) obj;
		return Float.floatToIntBits(this.value()) == Float.floatToIntBits(other.value());
	}

	@Override
	public String toString() {
		return String.format("SimplePercent [value=%s]", this.value);
	}
}
