package de.hetzge.tilemapgen.base.impl;

import de.hetzge.tilemapgen.base.def.Grid;
import de.hetzge.tilemapgen.base.def.Position;
import de.hetzge.tilemapgen.base.def.Size;
import javaslang.collection.Iterator;
import javaslang.control.Option;

/**
 * Just keeps the values at outline positions. A outline position is a position
 * where a value is around that is not equal.
 */
public class OutlineGrid<T> implements Grid<T> {

	private final Grid<T> grid;

	public OutlineGrid(Grid<T> grid) {
		this.grid = grid;
	}

	@Override
	public Size size() {
		return this.grid.size();
	}

	@Override
	public Option<T> get(Position position) {
		final Option<T> option = this.grid.get(position);
		if (option.isDefined()) {
			final boolean existOtherAround = Iterator.ofAll(new AdvancedAroundPositions(position, size())).exists(p -> !this.grid.get(p).eq(option));
			final boolean isBorder = size().isBorder(position);
			final boolean hasNonBorderAround = Iterator.ofAll(new AroundPositions(position, size())).exists(p -> !size().isBorder(p) && this.grid.get(p).eq(option));
			final boolean isEdge = size().isEdge(position);
			
			if((!isBorder && existOtherAround) || (isBorder && hasNonBorderAround) || isEdge) {
				return option;
			} else {
				return Option.none();
			}
		} else {
			return Option.none();
		}
	}

}
