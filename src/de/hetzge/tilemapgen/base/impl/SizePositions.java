package de.hetzge.tilemapgen.base.impl;

import java.util.Iterator;
import java.util.List;

import de.hetzge.tilemapgen.base.def.Position;
import de.hetzge.tilemapgen.base.def.Size;

public final class SizePositions implements Iterable<Position> {

	private final Size size;

	public SizePositions(Size size) {
		this.size = size;
	}

	public List<Position> toList() {
		return Utils.toList(this);
	}

	@Override
	public Iterator<Position> iterator() {
		return new Iterator<Position>() {
			private int i = 0;

			@Override
			public boolean hasNext() {
				return this.i < SizePositions.this.size.area();
			}

			@Override
			public Position next() {
				final float width = SizePositions.this.size.width();
				final int x = (int) (this.i % width);
				final int y = (int) Math.floor(this.i / width);

				this.i = this.i + 1;
				return new SimplePosition(x, y);
			}
		};
	}

}
