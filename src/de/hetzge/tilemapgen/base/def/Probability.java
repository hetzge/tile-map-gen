package de.hetzge.tilemapgen.base.def;

import java.util.Random;

public interface Probability<T> {

	Percent value(T t);

	default boolean is(T t, Random random) {
		return value(t).value() > random.nextFloat();
	}
}
