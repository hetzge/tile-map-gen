package de.hetzge.tilemapgen.base.def;

import java.util.Comparator;

import de.hetzge.tilemapgen.base.impl.SimplePercent;

public interface Percent {
	float value();

	default Percent inverse() {
		return new SimplePercent(1f - value());
	}

	public static class DefaultComparator implements Comparator<Percent> {
		@Override
		public int compare(Percent a, Percent b) {
			return Float.compare(a.value(), b.value());
		}
	}
}
