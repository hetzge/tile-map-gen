package de.hetzge.tilemapgen.base.def;

import javaslang.control.Option;

public interface Grid<T> {
	Size size();

	Option<T> get(Position position);
}
