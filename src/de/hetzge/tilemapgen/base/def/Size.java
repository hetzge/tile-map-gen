package de.hetzge.tilemapgen.base.def;

import de.hetzge.tilemapgen.base.impl.SimpleSize;

public interface Size {

	int width();

	int height();

	public default boolean contains(Position position) {
		return contains(new RootPosition(), position);
	}

	public default boolean contains(Position offset, Position position) {
		return position.x() < offset.x() + width() && offset.y() + position.y() < height() && position.x() >= offset.x() && position.y() >= offset.y();
	}

	public default int area() {
		return width() * height();
	}

	public default boolean isBorder(Position position) {
		final int x = position.x();
		final int y = position.y();
		return x == 0 || x == width() - 1 || y == 0 || y == height() - 1;
	}

	public default boolean isEdge(Position position) {
		final int x = position.x();
		final int y = position.y();
		final int width = width();
		final int height = height();
		return x == 0 && y == 0 || x == width - 1 && y == 0 || x == 0 && y == height - 1 || x == width - 1 && y == height - 1;
	}

	public default Size divide(Size size) {
		if (width() % size.width() != 0) {
			throw new IllegalArgumentException(String.format("The width must be divisable by '%s'", size.width()));
		}
		if (height() % size.height() != 0) {
			throw new IllegalArgumentException(String.format("The height must be divisable by '%s'", size.height()));
		}

		return new SimpleSize(width() / size.width(), height() / size.height());
	}
	
	public default Size multiply(Size size) {
		return new SimpleSize(width() * size.width(), height() * size.height());
	}

	static final class RootPosition implements Position {
		@Override
		public int y() {
			return 0;
		}

		@Override
		public int x() {
			return 0;
		}
	}

}
