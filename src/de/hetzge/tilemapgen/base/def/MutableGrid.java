package de.hetzge.tilemapgen.base.def;

public interface MutableGrid<T> extends Grid<T> {
	void set(Position position, T value);
}
