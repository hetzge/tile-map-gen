package de.hetzge.tilemapgen.base.def;

import de.hetzge.tilemapgen.base.impl.Direction;
import javaslang.control.Option;

/**
 * A grid of 3*3
 */
public interface Nine<T> extends Grid<T> {

	Option<T> get(Direction direction);

}
