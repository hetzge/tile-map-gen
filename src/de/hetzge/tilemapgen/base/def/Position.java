package de.hetzge.tilemapgen.base.def;

public interface Position {
	int x();

	int y();

	default int distance(Position otherPosition) {
		int a = Math.abs(x() - otherPosition.x());
		int b = Math.abs(y() - otherPosition.y());
		return (int) Math.floor(Math.sqrt(a * a + b * b));
	}
}
