package de.hetzge.tilemapgen.base.def;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public interface Format<T> {
	void write(OutputStream out, T t) throws IOException;

	default void writeToFile(File file, T t) {
		try (FileOutputStream out = new FileOutputStream(file)) {
			write(out, t);
		} catch (IOException e) {
			throw new IllegalStateException("Error while write tile map to file", e);
		}
	}
}
