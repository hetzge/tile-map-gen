package de.hetzge.tilemapgen.base.impl;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import de.hetzge.tilemapgen.base.def.Position;

public class SizePositionsTest {

	@Test
	public void test() {
		final SizePositions sizePositions = new SizePositions(new SimpleSize(3));

		final List<Position> expected = Arrays.asList(new SimplePosition(0, 0), new SimplePosition(1, 0), new SimplePosition(2, 0), new SimplePosition(0, 1), new SimplePosition(1, 1), new SimplePosition(2, 1), new SimplePosition(0, 2), new SimplePosition(1, 2), new SimplePosition(2, 2));
		final List<Position> actual = Utils.toList(sizePositions.iterator());
		assertEquals(expected, actual);
	}
}
