package de.hetzge.tilemapgen.base.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import de.hetzge.tilemapgen.base.def.MutableGrid;
import de.hetzge.tilemapgen.map.def.Zone;
import de.hetzge.tilemapgen.map.impl.SimpleZone;

public class SimpleMutableGridTest {

	@Test
	public void test() {
		final Zone zoneA = new SimpleZone("A");
		final Zone zoneB = new SimpleZone("B");
		final Zone zoneC = new SimpleZone("C");

		final SimplePosition positionA = new SimplePosition(0, 0);
		final SimplePosition positionB = new SimplePosition(1, 0);
		final SimplePosition positionC = new SimplePosition(2, 0);

		final MutableGrid<Zone> grid = new SimpleMutableGrid<>(new SimpleSize(3));
		grid.set(positionA, zoneA);
		grid.set(positionB, zoneB);
		grid.set(positionC, zoneC);

		assertEquals(grid.get(positionA).get(), zoneA);
		assertEquals(grid.get(positionB).get(), zoneB);
		assertEquals(grid.get(positionC).get(), zoneC);
	}
}
