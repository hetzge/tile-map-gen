package de.hetzge.tilemapgen.base.impl;

import org.junit.Assert;
import org.junit.Test;

public class BasePositionTest {

	@Test
	public void testEquals() {
		Assert.assertEquals(new SimplePosition(1, 2), new AdditionPosition(new SimplePosition(1, 1), new SimplePosition(0, 1)));
	}
	
	@Test
	public void testHashCode() {
		Assert.assertEquals(new SimplePosition(1, 2).hashCode(), new AdditionPosition(new SimplePosition(1, 1), new SimplePosition(0, 1)).hashCode());
	}

}
