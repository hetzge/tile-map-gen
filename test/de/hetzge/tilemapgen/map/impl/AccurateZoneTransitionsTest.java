package de.hetzge.tilemapgen.map.impl;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

import de.hetzge.tilemapgen.base.impl.SimplePercent;

public class AccurateZoneTransitionsTest {

	@Test
	public void test() {
		final SimpleZone a = new SimpleZone("A");
		final SimpleZone b = new SimpleZone("B");
		final SimpleZone c = new SimpleZone("C");
		final SimpleZone d = new SimpleZone("D");

		final AccurateZoneTransitions zoneTransitions = new AccurateZoneTransitions(Arrays.asList(a, b, c, d));
		assertEquals(a, zoneTransitions.zone(new SimplePercent(0f)).get());
		assertEquals(b, zoneTransitions.zone(new SimplePercent(0.25f)).get());
		assertEquals(c, zoneTransitions.zone(new SimplePercent(0.5f)).get());
		assertEquals(d, zoneTransitions.zone(new SimplePercent(0.75f)).get());
		assertEquals(d, zoneTransitions.zone(new SimplePercent(1f)).get());
	}

}
