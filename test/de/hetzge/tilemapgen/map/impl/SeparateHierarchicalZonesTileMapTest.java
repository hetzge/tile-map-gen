package de.hetzge.tilemapgen.map.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.junit.Test;

import de.hetzge.tilemapgen.base.def.MutableGrid;
import de.hetzge.tilemapgen.base.impl.SimpleMutableGrid;
import de.hetzge.tilemapgen.base.impl.SimplePosition;
import de.hetzge.tilemapgen.base.impl.SimpleSize;
import de.hetzge.tilemapgen.map.def.Layer;
import de.hetzge.tilemapgen.map.def.TileMap;
import de.hetzge.tilemapgen.map.def.Zone;

public class SeparateHierarchicalZonesTileMapTest {

	@Test
	public void test() {
		final Zone zoneA = new SimpleZone("A");
		final Zone zoneB = new SimpleZone("B");
		final Zone zoneC = new SimpleZone("C");
		final List<Zone> zones = Arrays.asList(zoneA, zoneB, zoneC);

		final SimplePosition positionA = new SimplePosition(0, 0);
		final SimplePosition positionB = new SimplePosition(1, 0);
		final SimplePosition positionC = new SimplePosition(2, 0);

		final MutableGrid<Zone> grid = new SimpleMutableGrid<>(new SimpleSize(10));
		grid.set(positionA, zoneA);
		grid.set(positionB, zoneB);
		grid.set(positionC, zoneC);
		
		final Comparator<Zone> comparator = (a, b) -> {
			return a.key().compareTo(b.key());
		};
		
		ArrayList<Zone> arrayList = new ArrayList<>(zones);
		arrayList.sort(comparator);
		
		final TileMap tileMap = new SeparateHierarchicalZonesTileMap(zones, grid, comparator);

		final List<Layer> layers = tileMap.layers();
		
		assertEquals(3, layers.size());

		assertTrue(layers.get(0).is(positionA));
		assertFalse(layers.get(1).is(positionA));
		assertFalse(layers.get(2).is(positionA));

		assertTrue(layers.get(0).is(positionB));
		assertTrue(layers.get(1).is(positionB));
		assertFalse(layers.get(2).is(positionB));

		assertTrue(layers.get(0).is(positionC));
		assertTrue(layers.get(1).is(positionC));
		assertTrue(layers.get(2).is(positionC));
	}
}
