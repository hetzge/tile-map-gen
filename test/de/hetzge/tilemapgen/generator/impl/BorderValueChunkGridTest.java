package de.hetzge.tilemapgen.generator.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Random;
import java.util.function.Function;

import org.junit.Test;

import de.hetzge.tilemapgen.base.def.Grid;
import de.hetzge.tilemapgen.base.def.Percent;
import de.hetzge.tilemapgen.base.def.Position;
import de.hetzge.tilemapgen.base.impl.SimplePercent;
import de.hetzge.tilemapgen.base.impl.SimplePosition;
import de.hetzge.tilemapgen.base.impl.SimpleSize;
import de.hetzge.tilemapgen.generator.def.Chunk;

public class BorderValueChunkGridTest {

	@Test
	public void test() {
		final Percent value = new SimplePercent(0.5f);
		final BorderValueChunkGrid grid = new BorderValueChunkGrid(new SimpleSize(10), value, new Random(123L));

		verify(grid, new SimplePosition(0, 0), value, Chunk::topLeft);
		verify(grid, new SimplePosition(0, 0), value, Chunk::topRight);
		verify(grid, new SimplePosition(0, 0), value, Chunk::bottomLeft);
		
		verify(grid, new SimplePosition(9, 0), value, Chunk::topRight);
		verify(grid, new SimplePosition(9, 0), value, Chunk::topLeft);
		verify(grid, new SimplePosition(9, 0), value, Chunk::bottomRight);
		
		verify(grid, new SimplePosition(0, 9), value, Chunk::bottomLeft);
		verify(grid, new SimplePosition(0, 9), value, Chunk::bottomRight);
		verify(grid, new SimplePosition(0, 9), value, Chunk::topLeft);
		
		verify(grid, new SimplePosition(9, 9), value, Chunk::bottomRight);
		verify(grid, new SimplePosition(9, 9), value, Chunk::bottomLeft);
		verify(grid, new SimplePosition(9, 9), value, Chunk::topRight);

		verify(grid, new SimplePosition(5, 9), value, Chunk::bottomLeft);
		verify(grid, new SimplePosition(5, 9), value, Chunk::bottomRight);
		
		verify(grid, new SimplePosition(9, 5), value, Chunk::topRight);
		verify(grid, new SimplePosition(9, 5), value, Chunk::bottomRight);
		
		verify(grid, new SimplePosition(5, 0), value, Chunk::topLeft);
		verify(grid, new SimplePosition(5, 0), value, Chunk::topRight);
		
		verify(grid, new SimplePosition(0, 5), value, Chunk::topLeft);
		verify(grid, new SimplePosition(0, 5), value, Chunk::bottomLeft);

		assertTrue(grid.get(new SimplePosition(5, 5)).isEmpty());
	}

	private void verify(Grid<Chunk> grid, Position position, Percent value, Function<Chunk, Percent> f) {
		assertTrue(grid.get(position).isDefined());
		assertEquals(value, f.apply(grid.get(position).get()));
	}

}
