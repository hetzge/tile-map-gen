package de.hetzge.tilemapgen.generator.impl;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.junit.Test;

import de.hetzge.tilemapgen.base.def.Grid;
import de.hetzge.tilemapgen.base.def.Percent;
import de.hetzge.tilemapgen.base.def.Position;
import de.hetzge.tilemapgen.base.impl.SimplePercent;
import de.hetzge.tilemapgen.base.impl.SimplePosition;
import de.hetzge.tilemapgen.base.impl.SimpleSize;
import de.hetzge.tilemapgen.base.impl.SizePositions;
import de.hetzge.tilemapgen.generator.def.Chunk;
import de.hetzge.tilemapgen.map.def.Zone;
import de.hetzge.tilemapgen.map.def.ZoneTransitions;
import de.hetzge.tilemapgen.map.impl.AccurateZoneTransitions;
import de.hetzge.tilemapgen.map.impl.SimpleZone;
import de.hetzge.tilemapgen.map.impl.ValueZoneGrid;

public class ChunkGridTest {

	private static final SimpleZone ZONE_A = new SimpleZone("A");
	private static final SimpleZone ZONE_B = new SimpleZone("B");
	private static final SimpleZone ZONE_C = new SimpleZone("C");
	private static final SimpleZone ZONE_D = new SimpleZone("D");
	private static final List<Zone> ZONES = Arrays.asList(ZONE_A, ZONE_B, ZONE_C, ZONE_D);

	@Test
	public void testChunkGrid() {
		final Grid<Chunk> chunkGrid = new ChunkGrid(new SimpleSize(5), new Random(1));

		final Chunk chunk = chunkGrid.get(new SimplePosition(1, 1)).get();
		final Chunk rightChunk = chunkGrid.get(new SimplePosition(2, 1)).get();

		assertEquals(chunk.topRight(), rightChunk.topLeft());
		assertEquals(chunk.bottomRight(), rightChunk.bottomLeft());

		final Chunk bottomChunk = chunkGrid.get(new SimplePosition(1, 2)).get();

		assertEquals(chunk.bottomLeft(), bottomChunk.topLeft());
		assertEquals(chunk.bottomRight(), bottomChunk.topRight());

		final Chunk leftBottomChunk = chunkGrid.get(new SimplePosition(2, 2)).get();

		assertEquals(chunk.bottomRight(), leftBottomChunk.topLeft());
	}

	@Test
	public void testZoneTransitions() {
		final ZoneTransitions zoneTransitions = new AccurateZoneTransitions(ZONES);

		assertEquals(ZONE_A, zoneTransitions.zone(new SimplePercent(0f)).get());
		assertEquals(ZONE_A, zoneTransitions.zone(new SimplePercent(0.249f)).get());
		assertEquals(ZONE_B, zoneTransitions.zone(new SimplePercent(0.250f)).get());
		assertEquals(ZONE_B, zoneTransitions.zone(new SimplePercent(0.499f)).get());
		assertEquals(ZONE_D, zoneTransitions.zone(new SimplePercent(0.999f)).get());
	}

	@Test
	public void testChunkZoneGridA() {
		final ZoneTransitions zoneTransitions = new AccurateZoneTransitions(ZONES);
		final Grid<Percent> valueGrid = new ChunkValueGrid(new SimpleSize(5), new SimpleChunk(new SimplePercent(0f), new SimplePercent(0.250f), new SimplePercent(0.250f), new SimplePercent(0.250f)));
		final Grid<Zone> zoneGrid = new ValueZoneGrid(valueGrid, zoneTransitions);

		assertEquals(ZONE_A, zoneGrid.get(new SimplePosition(0, 0)).get());
		assertEquals(ZONE_B, zoneGrid.get(new SimplePosition(4, 0)).get());
		assertEquals(ZONE_B, zoneGrid.get(new SimplePosition(0, 4)).get());
		assertEquals(ZONE_A, zoneGrid.get(new SimplePosition(0, 3)).get());
		assertEquals(ZONE_A, zoneGrid.get(new SimplePosition(3, 3)).get());
	}

	@Test
	public void testChunkZoneGridB() {
		final ZoneTransitions zoneTransitions = new AccurateZoneTransitions(ZONES);
		final Grid<Percent> valueGrid = new ChunkValueGrid(new SimpleSize(5), new SimpleChunk(new SimplePercent(0.250f), new SimplePercent(0.750f), new SimplePercent(0.750f), new SimplePercent(0.250f)));
		final Grid<Zone> zoneGrid = new ValueZoneGrid(valueGrid, zoneTransitions);

		assertEquals(ZONE_C, zoneGrid.get(new SimplePosition(2, 2)).get());
		assertEquals(ZONE_C, zoneGrid.get(new SimplePosition(1, 3)).get());
		assertEquals(ZONE_C, zoneGrid.get(new SimplePosition(3, 1)).get());
		assertEquals(ZONE_B, zoneGrid.get(new SimplePosition(1, 1)).get());
	}

	@Test
	public void testChunkZoneGridC() {
		final ZoneTransitions zoneTransitions = new AccurateZoneTransitions(ZONES);
		final Grid<Percent> valueGrid = new ChunkValueGrid(new SimpleSize(100), new SimpleChunk(new SimplePercent(0.250f), new SimplePercent(0.250f), new SimplePercent(0.250f), new SimplePercent(0.250f)));
		final Grid<Zone> zoneGrid = new ValueZoneGrid(valueGrid, zoneTransitions);

		for (Position position : new SizePositions(zoneGrid.size())) {
			assertEquals(String.format("position: %s", position), ZONE_B, zoneGrid.get(position).get());
		}
	}
}
